import "reflect-metadata";
import Client from "../../../src/domain/models/client";
import Result from "../../../src/domain/result/result";
import { ResultStatus } from "../../../src/domain/result/result-status.enum";
import ClientUpdateUseCase from "../../../src/domain/usecase/client/client_update_use_case";
import { MockClientRepository } from "../../instrument/domain/client_repository.mock";

describe('Client Update Use Case', () => {
  let mockClientRepository: MockClientRepository;

  beforeEach(() => {
    mockClientRepository = new MockClientRepository();
  });

  test('Client Update user case success', async () => {
    //Arrange
    var clientUpdateUseCase: ClientUpdateUseCase = new ClientUpdateUseCase(mockClientRepository);
    const mockClientUpdate = jest.spyOn(mockClientRepository, 'clientUpdate');
    const clientToUpdate: Client = { id: 1, firstName: 'Update', lastName: 'User' };
    const mockRepositoryResult: Result<Client> = {
      status: ResultStatus.ok,
      data: clientToUpdate
    }
    mockClientUpdate.mockImplementation(async () => mockRepositoryResult);

    //Act
    const result = await clientUpdateUseCase.invoke(clientToUpdate);

    //Assert
    expect(result.status).toBe(ResultStatus.ok);
    expect(result.data).toBe(clientToUpdate);
    expect(result.error).toBeUndefined();
  });

  test('Client Update user case error', async () => {
    //Arrange
    var clientUpdateUseCase: ClientUpdateUseCase = new ClientUpdateUseCase(mockClientRepository);
    const mockClientUpdate = jest.spyOn(mockClientRepository, 'clientUpdate');
    const clientToUpdate: Client = { id: 1, firstName: 'Update', lastName: 'User' };
    const mockRepositoryResult: Result<Client> = {
      status: ResultStatus.fail,
      data: clientToUpdate,
      error: 'Network error'
    }
    mockClientUpdate.mockImplementation(async () => mockRepositoryResult);

    //Act
    const useCaseResult = await clientUpdateUseCase.invoke(clientToUpdate);

    //Assert
    expect(useCaseResult.status).toBe(ResultStatus.fail);
    expect(useCaseResult.error).toBe('Network error');
    expect(mockClientUpdate).toHaveBeenCalled();
    expect(mockClientUpdate).toBeCalledTimes(1);
  });
});

