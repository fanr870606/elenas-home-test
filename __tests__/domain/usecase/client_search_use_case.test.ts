import "reflect-metadata";
import Client from "../../../src/domain/models/client";
import Result from "../../../src/domain/result/result";
import { ResultStatus } from "../../../src/domain/result/result-status.enum";
import ClientSearchUseCase from "../../../src/domain/usecase/client/client_search_use_case";
import { MockClientRepository } from "../../instrument/domain/client_repository.mock";

describe('Client Search Use Case', () => {
  let mockClientRepository: MockClientRepository;

  beforeEach(() => {
    mockClientRepository = new MockClientRepository();
  });

  test('Client search user case success', async () => {
    //Arrange
    var clientSearchUseCase: ClientSearchUseCase = new ClientSearchUseCase(mockClientRepository);
    const mockClientSearch = jest.spyOn(mockClientRepository, 'clientSearch');
    const resultWithClients: Result<Client[]> = {
      status: ResultStatus.ok,
      data: [{ id: 1 }, { id: 2 }]
    }
    mockClientSearch.mockImplementation(async () => resultWithClients);

    //Act
    const result = await clientSearchUseCase.invoke();

    ///Asert
    expect(result.status).toBe(ResultStatus.ok);
    expect(result.data?.length).toBe(2);
    expect(result.error).toBeUndefined();
  });

  test('Client search user case error', async () => {
    //Arrange
    var clientSearchUseCase: ClientSearchUseCase = new ClientSearchUseCase(mockClientRepository);
    const mockClientSearch = jest.spyOn(mockClientRepository, 'clientSearch');
    const resultWithClients: Result<Client[]> = {
      status: ResultStatus.fail,
      data: [],
      error: 'Network error'
    }
    mockClientSearch.mockImplementation(async () => resultWithClients);

    //Act
    const result = await clientSearchUseCase.invoke();

    ///Asert
    expect(result.status).toBe(ResultStatus.fail);
    expect(result.data?.length).toBe(0);
    expect(result.error).toBe('Network error');
    expect(mockClientSearch).toHaveBeenCalled();
    expect(mockClientSearch).toBeCalledTimes(1);
  });
});

