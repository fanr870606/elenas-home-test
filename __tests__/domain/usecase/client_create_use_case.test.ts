import "reflect-metadata";
import Client from "../../../src/domain/models/client";
import Result from "../../../src/domain/result/result";
import { ResultStatus } from "../../../src/domain/result/result-status.enum";
import ClientCreateUseCase from "../../../src/domain/usecase/client/client_create_use_case";
import { MockClientRepository } from "../../instrument/domain/client_repository.mock";

describe('Client Create Use Case', () => {
  let mockClientRepository: MockClientRepository;

  beforeEach(() => {
    mockClientRepository = new MockClientRepository();
  });

  test('Client Create user case success', async () => {
    //Arrange
    var clientCreateUseCase: ClientCreateUseCase = new ClientCreateUseCase(mockClientRepository);
    const mockClientCreate = jest.spyOn(mockClientRepository, 'clientCreate');
    const clientToCreate: Client = { id: 1, firstName: 'Create', lastName: 'User' };
    const mockRepositoryResult: Result<Client> = {
      status: ResultStatus.ok,
      data: clientToCreate
    }
    mockClientCreate.mockImplementation(async () => mockRepositoryResult);

    //Act
    const result = await clientCreateUseCase.invoke(clientToCreate);

    //Assert
    expect(result.status).toBe(ResultStatus.ok);
    expect(result.data).toBe(clientToCreate);
    expect(result.error).toBeUndefined();
  });

  test('Client Create user case error', async () => {
    //Arrange
    var clientCreateUseCase: ClientCreateUseCase = new ClientCreateUseCase(mockClientRepository);
    const mockClientCreate = jest.spyOn(mockClientRepository, 'clientCreate');
    const clientToCreate: Client = { id: 1, firstName: 'Create', lastName: 'User' };
    const mockRepositoryResult: Result<Client> = {
      status: ResultStatus.fail,
      data: clientToCreate,
      error: 'Network error'
    }
    mockClientCreate.mockImplementation(async () => mockRepositoryResult);

    //Act
    const useCaseResult = await clientCreateUseCase.invoke(clientToCreate);

    //Assert
    expect(useCaseResult.status).toBe(ResultStatus.fail);
    expect(useCaseResult.error).toBe('Network error');
    expect(mockClientCreate).toHaveBeenCalled();
    expect(mockClientCreate).toBeCalledTimes(1);
  });
});

