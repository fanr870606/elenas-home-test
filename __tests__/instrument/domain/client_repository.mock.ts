import client from "../../../src/domain/models/client";
import ClientRepositoryContract from "../../../src/domain/repositories/client_repository_contract";
import result from "../../../src/domain/result/result";

export class MockClientRepository implements ClientRepositoryContract {
  clientSearch(): Promise<result<client[]>> {
    throw new Error("Method not implemented.");
  }
  clientUpdate(client: client): Promise<result<client>> {
    throw new Error("Method not implemented.");
  }
  clientCreate(client: client): Promise<result<client>> {
    throw new Error("Method not implemented.");
  }
}