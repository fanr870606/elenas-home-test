import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font';
import React from 'react';
import renderer from 'react-test-renderer';
import Client from '../../../src/domain/models/client';
import ClientCard from '../../../src/ui/components/client-card/client-card.component';
import { shallow } from 'enzyme'
import { TouchableOpacity } from 'react-native';


describe('<ClientCard />', () => {
  const client: Client = {
    id: 1,
    firstName: "test",
    lastName: "user"
  }

  beforeEach(async () => {
    await Font.loadAsync({
      ...Ionicons.font,
      'space-mono': require('../../../src/ui/assets/fonts/SpaceMono-Regular.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
  });

  test('renders correctly', () => {
    const component = <ClientCard client={client} selectClient={(client: Client) => { }} />;
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('select client to edit', () => {
    const clientSelectedMock = jest.fn();
    const onSelectClientMock = jest.fn((selectedClient: Client) => {
      clientSelectedMock();
      return selectedClient;
    });
    const component = <ClientCard client={client} selectClient={onSelectClientMock} />;
    const wrapper = shallow(component);
    const button = wrapper.find(TouchableOpacity).first();
    expect(button.type()).toEqual(TouchableOpacity)

    button.simulate('press');
    expect(clientSelectedMock).toHaveBeenCalled();
    expect(clientSelectedMock).toBeCalledTimes(1);
    expect(onSelectClientMock).toBeCalledWith(client);
  });
});