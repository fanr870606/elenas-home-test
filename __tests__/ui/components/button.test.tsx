import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme'
import CustomButton from '../../../src/ui/components/custom-button/custom-button.component';
import { Spinner } from 'native-base';

describe('<CustomButton />', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<CustomButton onPress={() => { }} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('show spinner while executing', () => {
    //Arrange
    const component = <CustomButton isExecuting onPress={() => { }} />;
    const wrapper = shallow(component);
    const spinner = wrapper.find(Spinner);

    //Assert
    expect(spinner.length).toEqual(1)
  });

  test('hide spinner while not executing', () => {
    //Arrange
    const component = <CustomButton onPress={() => { }} />;
    const wrapper = shallow(component);
    const spinner = wrapper.find(Spinner);

    //Assert
    expect(spinner.length).toBe(0);
  });
});