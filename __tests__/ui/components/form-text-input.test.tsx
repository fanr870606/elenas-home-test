import React from 'react';
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer';
import FormTextInput from '../../../src/ui/components/form-text-input/form-text-input.component';
import { Label } from 'native-base';

describe('<FormTextInput />', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<FormTextInput />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('show label if param exists', () => {
    //Arrange
    const component = <FormTextInput label={"Title"} />;
    const wrapper = shallow(component);
    const label = wrapper.find(Label);

    //Assert
    expect(label.length).toEqual(1)
  });

  test('hide label if param not exists', () => {
    //Arrange
    const component = <FormTextInput />;
    const wrapper = shallow(component);
    const label = wrapper.find(Label);

    //Assert
    expect(label.length).toBe(0);
  });
});