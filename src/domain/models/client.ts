export default interface Client {
  id: number
  registerDate?: Date,
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: string,
  city?: string,
  cellphone?: string,
  email?: string,
}
