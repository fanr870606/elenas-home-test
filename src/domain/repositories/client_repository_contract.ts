import Client from "../models/client";
import Result from "../result/result";

export default interface ClientRepositoryContract {
  clientSearch(): Promise<Result<Client[]>>;
  clientUpdate(client: Client): Promise<Result<Client>>;
  clientCreate(client: Client): Promise<Result<Client>>;
}