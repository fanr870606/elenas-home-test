import { IBaseUseCase } from "../base/base_use_case";
import Client from "../../models/client"
import Result from "../../result/result";
import ClientRepositoryContract from "../../repositories/client_repository_contract";
import { TYPES } from "../../../dependency-injection/types";
import { inject, injectable } from "inversify";

export interface ClientSearchUseCaseContract {
  invoke(): Promise<Result<Client[]>>;
}

@injectable()
export default class ClientSearchUseCase implements IBaseUseCase<any, Client[]>, ClientSearchUseCaseContract {

  private _repository: ClientRepositoryContract;

  public constructor(
    @inject(TYPES.ClientRepositoryContract) repository: ClientRepositoryContract,
  ) {
    this._repository = repository;
  }

  async invoke(): Promise<Result<Client[]>> {
    return await this._repository.clientSearch();
  }
}