import { IBaseUseCase } from "../base/base_use_case";
import Client from "../../models/client"
import Result from "../../result/result";
import ClientRepositoryContract from "../../repositories/client_repository_contract";
import { TYPES } from "../../../dependency-injection/types";
import { inject, injectable } from "inversify";

export interface ClientCreateUseCaseContract {
  invoke(client: Client): Promise<Result<Client>>;
}

@injectable()
export default class ClientCreateUseCase implements IBaseUseCase<Client, Client>, ClientCreateUseCaseContract {

  private _repository: ClientRepositoryContract;

  public constructor(
    @inject(TYPES.ClientRepositoryContract) repository: ClientRepositoryContract,
  ) {
    this._repository = repository;
  }

  async invoke(client: Client): Promise<Result<Client>> {
    return await this._repository.clientCreate(client);
  }
}