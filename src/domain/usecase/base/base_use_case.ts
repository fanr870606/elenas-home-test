import Result from "../../result/result";

export interface IBaseUseCase<P, T> {
  invoke(params: P): Promise<Result<T>>;
}