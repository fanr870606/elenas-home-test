import { IValidationError } from "../../data/models/error.response";
import { ResultStatus } from "./result-status.enum";

export default class Result<T> {
  status?: ResultStatus;
  data?: T;
  error?: string;

  constructor({ data, status, error }: { data: T, status?: ResultStatus, error?: string }) {
    this.status = status;
    this.data = data;
    this.error = error;
    if (!!error) {
      this.status = ResultStatus.fail;
    }
  }
}