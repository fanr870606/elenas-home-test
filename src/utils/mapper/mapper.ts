export default function map<S, D>(objectToMap: S, propertiesAllowed: string[]): D {
  let objectMapped: any = {};

  let properties = Object.keys(objectToMap);
  properties.forEach(prop => {
    if (propertiesAllowed.length == 0
      || propertiesAllowed.includes(prop)) {
      objectMapped[prop] = getKeyValue(prop)(objectToMap);;
    }
  });

  return objectMapped as D;
}


const getKeyValue = (key: string) => (obj: Record<string, any>) => obj[key];