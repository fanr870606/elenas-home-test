
import { inject, injectable } from "inversify";
import { TYPES } from "../dependency-injection/types";
import Client from "../domain/models/client";
import ClientRepositoryContract from "../domain/repositories/client_repository_contract";
import Result from "../domain/result/result";
import { ResultStatus } from "../domain/result/result-status.enum";
import map from "../utils/mapper/mapper";
import ClientRemoteDatasourceContract from "./contracts/network/client_remote_datasource_contract";
import { IClientCreateRequest } from "./models/client-create.request";
import { IClientSearchRequest } from "./models/client-search.request";
import { IClientSearchResult } from "./models/client-search.response";
import { IClientUpdateRequest } from "./models/client-update.request";
import { IValidationError } from "./models/error.response";

@injectable()
class ConcreteClientRepository implements ClientRepositoryContract {
  private _remoteDatasource: ClientRemoteDatasourceContract;

  public constructor(
    @inject(TYPES.ClientRemoteDatasourceContract) remoteDatasource: ClientRemoteDatasourceContract,
  ) {
    this._remoteDatasource = remoteDatasource;
  }

  async clientSearch(): Promise<Result<Client[]>> {
    var request: IClientSearchRequest = {
      page: 0,
      perPage: 100
    };

    var response = await this._remoteDatasource.clientSearch(request);
    if (response.data && response.data.clientsSearch && Array.isArray(response.data.clientsSearch.results)) {
      var clients: Client[] = response.data.clientsSearch.results.map(item => map<IClientSearchResult, Client>(item, []));
      return new Result({ data: clients, status: ResultStatus.ok });
    } else {
      return new Result({ data: [], status: ResultStatus.fail });
    }
  }

  async clientUpdate(client: Client): Promise<Result<Client>> {
    var request: IClientUpdateRequest = {
      id: client.id,
      input: {
        firstName: client.firstName,
        lastName: client.lastName,
        cellphone: client.cellphone,
        cedula: client.cedula,
        address: {
          streetAddress: client.address
        }
      },
    };
    var response = await this._remoteDatasource.clientUpdate(request);
    if (response.data && response.data.updateClient && !response.data.updateClient.errors) {
      return new Result({ data: response.data.updateClient, status: ResultStatus.ok });
    } else {
      return new Result({ data: { id: 0 }, status: ResultStatus.fail, error: this.combineErrors(response.data.updateClient.errors) });
    }
  }

  async clientCreate(client: Client): Promise<Result<Client>> {
    var request: IClientCreateRequest = {
      id: client.id,
      input: {
        firstName: client.firstName,
        lastName: client.lastName,
        cellphone: client.cellphone,
        cedula: client.cedula,
        address: {
          streetAddress: client.address,
          cityId: 438,
          city: "Istmina",
          stateId: 15,
          stateShortCode: "CHO"
        }
      },
    };
    var response = await this._remoteDatasource.clientCreate(request);
    if (response.data && response.data.createClient && !response.data.createClient.errors) {
      return new Result({ data: response.data.createClient, status: ResultStatus.ok });
    } else {
      return new Result({ data: { id: 0 }, status: ResultStatus.fail, error: this.combineErrors(response.data.createClient.errors) });
    }
  }

  combineErrors = (errors?: IValidationError[]): string => {
    let error = '';
    if (errors && errors?.length > 0) {
      errors.forEach((value) => {
        error = error.concat(value.message ?? '');
      });
    }
    return error
  }
}

export default ConcreteClientRepository;