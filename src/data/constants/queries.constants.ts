export const CLIENT_SEARCH_OPERATION_NAME = 'ClientSearch';

export const CLIENT_SEARCH_QUERY = `
query ClientSearch($page: Int, $perPage: Int) {
  clientsSearch(page: $page, perPage: $perPage){
    results{
      id,
      registerDate,     
      firstName,
      lastName,         
      cedula,
      address,
      city,
      cellphone,
      state{
        id,
        displayName
      },
      credit
    }
  }
}`;

export const CLIENT_UPDATE_OPERATION_NAME = 'UpdateClient';

export const CLIENT_UPDATE_QUERY = `
mutation UpdateClient($id: Int!, $input: ClientInput!)
{
  updateClient(id: $id, input: $input)
  {        
    ... on Client {
      id,
      registerDate,     
      firstName,
      lastName,         
      cedula,
      address,
      city,
      cellphone,      
      credit
    }
    ... on ValidationErrors {
      message, 
      errors{
        message
      }
    }         
  }
}`;

export const CLIENT_CREATE_OPERATION_NAME = 'CreateClient';

export const CLIENT_CREATE_QUERY = `
mutation CreateClient($input: ClientInput!)
{
  createClient(input: $input)
  {        
    ... on Client {
      id,
      registerDate,     
      firstName,
      lastName,         
      cedula,
      address,
      city,      
      cellphone,      
      credit,
      state{
        id,
        shortCode,        
      }
    }
    ... on ValidationErrors {
      message, 
      errors{
        message
      }
    }         
  }
}`;
