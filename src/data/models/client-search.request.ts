export interface IClientSearchRequest {
  page: number,
  perPage: number
}