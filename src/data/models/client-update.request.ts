export interface IClientUpdateRequest {
  id: number,
  input: IClientUpdateInputRequest
}

export interface IClientUpdateInputRequest {
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: IClientUpdateAddressRequest,
  cellphone?: string,
}

export interface IClientUpdateAddressRequest {
  streetAddress?: string,
}
