import { IValidationError } from "./error.response";

export interface IClientCreateResponse {
  data: {
    createClient: IClientCreateResult
  },
  errors?: IValidationError[]
}

export interface IClientCreateResult {
  id: number
  registerDate?: Date,
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: string,
  city?: string,
  cellphone?: string,
  message?: string,
  errors?: IValidationError[]
}
