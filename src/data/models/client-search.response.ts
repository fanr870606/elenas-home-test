export interface IClientSearchResponse {
  data: {
    clientsSearch: {
      results: IClientSearchResult[]
    }
  }
}

export interface IClientSearchResult {
  id: number
  registerDate?: Date,
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: string,
  city?: string,
  cellphone?: string,
}