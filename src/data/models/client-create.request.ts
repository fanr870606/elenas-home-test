export interface IClientCreateRequest {
  id: number,
  input: IClientCreateInputRequest
}

export interface IClientCreateInputRequest {
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: IClientCreateAddressRequest,
  cellphone?: string,
}

export interface IClientCreateAddressRequest {
  streetAddress?: string,
  cityId?: number,
  city?: string,
  stateId?: number,
  stateShortCode?: string
}
