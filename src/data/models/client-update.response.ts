import { IValidationError } from "./error.response";

export interface IClientUpdateResponse {
  data: {
    updateClient: IClientUpdateResult
  },
  errors?: IValidationError[]
}

export interface IClientUpdateResult {
  id: number
  registerDate?: Date,
  firstName?: string,
  lastName?: string,
  cedula?: string,
  address?: string,
  city?: string,
  cellphone?: string,
  message?: string,
  errors?: IValidationError[]
}
