import axios, { AxiosResponse } from 'axios';
import { API_URL } from '../constants/api.constants';
import { CLIENT_CREATE_OPERATION_NAME, CLIENT_CREATE_QUERY, CLIENT_SEARCH_OPERATION_NAME, CLIENT_SEARCH_QUERY, CLIENT_UPDATE_OPERATION_NAME, CLIENT_UPDATE_QUERY } from '../constants/queries.constants';
import { IClientCreateRequest } from '../models/client-create.request';
import { IClientCreateResponse } from '../models/client-create.response';
import { IClientSearchRequest } from '../models/client-search.request';
import { IClientSearchResponse } from '../models/client-search.response';
import { IClientUpdateRequest } from '../models/client-update.request';
import { IClientUpdateResponse } from '../models/client-update.response';

axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

axios.interceptors.request.use(request => {
  return request
})

axios.interceptors.request.use(
  config => {
    const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiKzU3MzA1NzE5OTk5NSIsImlhdCI6MTYxOTMxOTAzMn0.HBZaMYoOlAB0vd-XCxQ6aNKSX8msM4B0LTWkz2fPbpA";
    config.headers.Authorization = `Token ${token}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
  get: (url: string) =>
    axios
      .get(url)
      .then(responseBody),
  post: async (operationName: string, query: string, variables: any) => {
    return axios
      .post(API_URL, {
        operationName,
        query,
        variables
      })
      .then(responseBody)
  },
  put: (url: string, body: {}) =>
    axios
      .put(url, body)
      .then(responseBody),
  del: (url: string) =>
    axios
      .delete(url)
      .then(responseBody),
};

const ClientEndpoints = {
  clientSearch: (clientSearchRequest: IClientSearchRequest): Promise<IClientSearchResponse> =>
    requests.post(CLIENT_SEARCH_OPERATION_NAME, CLIENT_SEARCH_QUERY, clientSearchRequest),
  clientUpdate: (clientSearchRequest: IClientUpdateRequest): Promise<IClientUpdateResponse> =>
    requests.post(CLIENT_UPDATE_OPERATION_NAME, CLIENT_UPDATE_QUERY, clientSearchRequest),
  clientCreate: (clientSearchRequest: IClientCreateRequest): Promise<IClientCreateResponse> =>
    requests.post(CLIENT_CREATE_OPERATION_NAME, CLIENT_CREATE_QUERY, clientSearchRequest),
};

export default {
  ClientEndpoints,
};
