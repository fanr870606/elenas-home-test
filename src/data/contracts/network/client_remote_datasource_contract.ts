import { IClientCreateRequest } from "../../models/client-create.request";
import { IClientCreateResponse } from "../../models/client-create.response";
import { IClientSearchRequest } from "../../models/client-search.request";
import { IClientSearchResponse } from "../../models/client-search.response";
import { IClientUpdateRequest } from "../../models/client-update.request";
import { IClientUpdateResponse } from "../../models/client-update.response";

export default interface ClientRemoteDatasourceContract {
  clientSearch(request: IClientSearchRequest): Promise<IClientSearchResponse>;
  clientUpdate(request: IClientUpdateRequest): Promise<IClientUpdateResponse>;
  clientCreate(request: IClientCreateRequest): Promise<IClientCreateResponse>;
}