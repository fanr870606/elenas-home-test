import { injectable } from "inversify";
import agent from "../../api/agent";
import ClientRemoteDatasourceContract from "../../contracts/network/client_remote_datasource_contract";
import { IClientCreateRequest } from "../../models/client-create.request";
import { IClientCreateResponse } from "../../models/client-create.response";
import { IClientSearchRequest } from "../../models/client-search.request";
import { IClientSearchResponse } from "../../models/client-search.response";
import { IClientUpdateRequest } from "../../models/client-update.request";
import { IClientUpdateResponse } from "../../models/client-update.response";

@injectable()
export default class ClientRemoteDatasource implements ClientRemoteDatasourceContract {
  async clientSearch(request: IClientSearchRequest): Promise<IClientSearchResponse> {
    return await agent.ClientEndpoints.clientSearch(request);
  }

  async clientUpdate(request: IClientUpdateRequest): Promise<IClientUpdateResponse> {
    return await agent.ClientEndpoints.clientUpdate(request);
  }

  async clientCreate(request: IClientCreateRequest): Promise<IClientCreateResponse> {
    return await agent.ClientEndpoints.clientCreate(request);
  }
}