import { Container } from "inversify";

import ConcreteClientRepository from "../data/client_repository";
import ClientRemoteDatasourceContract from "../data/contracts/network/client_remote_datasource_contract";
import ClientRemoteDatasource from "../data/datasources/network/client_remote_datasource";
import { TYPES } from "./types";
import ClientRepositoryContract from "../domain/repositories/client_repository_contract";
import ClientCreateUseCase, { ClientCreateUseCaseContract } from "../domain/usecase/client/client_create_use_case";
import ClientSearchUseCase, { ClientSearchUseCaseContract } from "../domain/usecase/client/client_search_use_case";
import ClientUpdateUseCase, { ClientUpdateUseCaseContract } from "../domain/usecase/client/client_update_use_case";

const diContainer = new Container();

diContainer.bind<ClientSearchUseCaseContract>(TYPES.ClientSearchUseCaseContract).to(ClientSearchUseCase);
diContainer.bind<ClientCreateUseCaseContract>(TYPES.ClientCreateUseCaseContract).to(ClientCreateUseCase);
diContainer.bind<ClientUpdateUseCaseContract>(TYPES.ClientUpdateUseCaseContract).to(ClientUpdateUseCase);

diContainer.bind<ClientRepositoryContract>(TYPES.ClientRepositoryContract).to(ConcreteClientRepository);

diContainer.bind<ClientRemoteDatasourceContract>(TYPES.ClientRemoteDatasourceContract).to(ClientRemoteDatasource);

export default diContainer;
