const TYPES = {
  ClientRepositoryContract: Symbol.for("ClientRepositoryContract"),
  ClientSearchUseCaseContract: Symbol.for("ClientSearchUseCaseContract"),
  ClientCreateUseCaseContract: Symbol.for("ClientCreateUseCaseContract"),
  ClientUpdateUseCaseContract: Symbol.for("ClientUpdateUseCaseContract"),
  ClientRemoteDatasourceContract: Symbol.for("ClientRemoteDatasourceContract"),
};

export { TYPES };