import React from 'react'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from '../redux/store';

const PersistProvider = (props: any) => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        {props.children}
      </PersistGate>
    </Provider>
  )
}

export default PersistProvider
