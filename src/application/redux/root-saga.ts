import { all, call, fork } from 'redux-saga/effects';

import { userSagas } from './auth/auth.sagas';
import { clientSagas } from './client/client.sagas';

export default function* rootSaga() {
  yield all([fork(userSagas), fork(clientSagas)]);
}
