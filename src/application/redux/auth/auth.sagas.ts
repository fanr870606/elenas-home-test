import { takeLatest, put, all, call, delay } from 'redux-saga/effects';
import AuthActionTypes from './auth.types';
import {
  signInSuccess, signInFailure, signOutFailure, signOutSuccess
} from './auth.actions';

export function* signIn(params: any) {
  try {
    const { payload }: { payload: any } = params;
    yield delay(200)
    // const user: IUserResponse = yield agent.User.login(payload);
    // if (user.hasError) {
    //   yield put(signInFailure(user.errorCode));
    // } else {
    //   yield put(signInSuccess(user));
    // }
  } catch (error) {
    yield put(signInFailure(error));
  }
}

export function* signOut() {
  try {
    yield put(signOutSuccess());
  } catch (error) {
    yield put(signOutFailure(error));
  }
}

export function* onSignInStart() {
  yield takeLatest(AuthActionTypes.AUTH_SIGN_IN_START, signIn);
}

export function* onSignOutStart() {
  yield takeLatest(AuthActionTypes.AUTH_SIGN_OUT_START, signOut);
}

export function* userSagas() {
  yield all([
    call(onSignInStart),
    call(onSignOutStart),
  ]);
}
