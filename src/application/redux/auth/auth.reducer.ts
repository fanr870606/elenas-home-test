import UserActionTypes from "./auth.types";

const INITIAL_STATE =
{
  isUserLoggedIn: false,
  isFetching: false,
  currentUser: null,
  error: null
}

const userReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case UserActionTypes.AUTH_SIGN_IN_START:
      return {
        ...state,
        isFetching: true
      };
    case UserActionTypes.AUTH_SIGN_IN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        currentUser: action.payload,
        isUserLoggedIn: true
      };
    case UserActionTypes.AUTH_SIGN_IN_FAILURE:
      return {
        ...state,
        isFetching: false,
        isUserLoggedIn: false,
        error: action.payload
      };
    case UserActionTypes.AUTH_SIGN_OUT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        currentUser: null,
        isUserLoggedIn: false
      };
    case UserActionTypes.AUTH_SIGN_OUT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.payload
      };
    default:
      return state;
  }
}

export default userReducer;