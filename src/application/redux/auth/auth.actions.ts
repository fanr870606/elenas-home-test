import UserActionTypes from "./auth.types"

export const signInStart = (user: any) => ({
    type: UserActionTypes.AUTH_SIGN_IN_START,
    payload: user
})

export const signInSuccess = (user: any) => ({
    type: UserActionTypes.AUTH_SIGN_IN_SUCCESS,
    payload: user
})

export const signInFailure = (error?: string) => ({
    type: UserActionTypes.AUTH_SIGN_IN_FAILURE,
    payload: error
})

export const signOutStart = () => ({
    type: UserActionTypes.AUTH_SIGN_OUT_START
})

export const signOutSuccess = () => ({
    type: UserActionTypes.AUTH_SIGN_OUT_SUCCESS
})

export const signOutFailure = (error?: string) => ({
    type: UserActionTypes.AUTH_SIGN_OUT_FAILURE,
    payload: error
})