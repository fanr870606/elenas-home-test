import { createSelector } from 'reselect';

const selectUser = (state: any) => state.user;

export const selectCurrentUser = createSelector(
  [selectUser],
  user => user.currentUser
);

export const selectIsUserFetching = createSelector(
  [selectUser],
  user => user.isFetching
);

export const selectIsUserLoggedIn = createSelector(
  [selectUser],
  user => user.isUserLoggedIn
);