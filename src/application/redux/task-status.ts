export enum TaskStatus {
  running,
  completed,
  failed,
  idle
}