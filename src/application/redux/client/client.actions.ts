import Client from "../../../domain/models/client"
import Result from "../../../domain/result/result"
import ClientActionTypes from "./client.types"

export const searchClientStart = () => ({
  type: ClientActionTypes.CLIENT_SEARCH_CLIENT_START
})

export const searchClientSuccess = (result: Result<Client[]>) => ({
  type: ClientActionTypes.CLIENT_SEARCH_CLIENT_SUCCESS,
  payload: result
})

export const searchClientFailure = (result: Result<Client[]>) => ({
  type: ClientActionTypes.CLIENT_SEARCH_CLIENT_FAILURE,
  payload: result
})

export const searchClientFinish = () => ({
  type: ClientActionTypes.CLIENT_SEARCH_CLIENT_FINISH
})

export const selectClient = (client: Client) => ({
  type: ClientActionTypes.CLIENT_SELECT_CLIENT,
  payload: client
})

export const updateClientStart = (client: Client) => ({
  type: ClientActionTypes.CLIENT_UPDATE_CLIENT_START,
  payload: client
})

export const updateClientSuccess = (result: Result<Client>) => ({
  type: ClientActionTypes.CLIENT_UPDATE_CLIENT_SUCCESS,
  payload: result
})

export const updateClientFailure = (result: Result<Client>) => ({
  type: ClientActionTypes.CLIENT_UPDATE_CLIENT_FAILURE,
  payload: result
})

export const updateClientFinish = () => ({
  type: ClientActionTypes.CLIENT_UPDATE_CLIENT_FINISH
})

export const createClientStart = (client: Client) => ({
  type: ClientActionTypes.CLIENT_CREATE_CLIENT_START,
  payload: client
})

export const createClientSuccess = (result: Result<Client>) => ({
  type: ClientActionTypes.CLIENT_CREATE_CLIENT_SUCCESS,
  payload: result
})

export const createClientFailure = (result: Result<Client>) => ({
  type: ClientActionTypes.CLIENT_CREATE_CLIENT_FAILURE,
  payload: result
})


export const createClientFinish = () => ({
  type: ClientActionTypes.CLIENT_CREATE_CLIENT_FINISH
})
