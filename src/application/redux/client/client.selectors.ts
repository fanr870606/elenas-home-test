import { createSelector } from 'reselect';

const selectClientState = (state: any) => state.client;

export const selectClientSearchTaskStatus = createSelector(
  [selectClientState],
  client => client.clientSearchTaskStatus
);

export const selectClients = createSelector(
  [selectClientState],
  client => client.clients
);

export const selectClientUpdateTaskStatus = createSelector(
  [selectClientState],
  client => client.clientUpdateTaskStatus
);

export const selectSelectedClient = createSelector(
  [selectClientState],
  client => client.selectedClient
);

export const selectClientCreateTaskStatus = createSelector(
  [selectClientState],
  client => client.clientCreateTaskStatus
);