import Client from "../../../domain/models/client";
import Result from "../../../domain/result/result";
import { ResultStatus } from "../../../domain/result/result-status.enum";
import { TaskStatus } from "../task-status";
import ClientActionTypes from "./client.types";

interface StateDefinition {
  clientSearchTaskStatus: TaskStatus,
  clients: Client[],
  clientUpdateTaskStatus: TaskStatus,
  selectedClient?: Client,
  clientCreateTaskStatus: TaskStatus,
}

const INITIAL_STATE: StateDefinition =
{
  clientSearchTaskStatus: TaskStatus.idle,
  clients: [],
  clientUpdateTaskStatus: TaskStatus.idle,
  selectedClient: undefined,
  clientCreateTaskStatus: TaskStatus.idle,
}

const clientReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case ClientActionTypes.CLIENT_SEARCH_CLIENT_START:
      return {
        ...state,
        clients: INITIAL_STATE.clients,
        clientSearchTaskStatus: TaskStatus.running,
      };
    case ClientActionTypes.CLIENT_SEARCH_CLIENT_SUCCESS:
      const searchResult: Result<Client[]> = action.payload;
      return {
        ...state,
        clientSearchTaskStatus: TaskStatus.completed,
        clients: searchResult.data
      };
    case ClientActionTypes.CLIENT_SEARCH_CLIENT_FAILURE:
      return {
        ...state,
        clientSearchTaskStatus: TaskStatus.failed,
        // error: action.payload,
      };
      case ClientActionTypes.CLIENT_SEARCH_CLIENT_FINISH:
        return {
          ...state,
          clientSearchTaskStatus: TaskStatus.idle,
          // error: action.payload,
        };
    case ClientActionTypes.CLIENT_SELECT_CLIENT:
      return {
        ...state,
        selectedClient: action.payload,
      };
    case ClientActionTypes.CLIENT_UPDATE_CLIENT_START:
      return {
        ...state,
        clientUpdateTaskStatus: TaskStatus.running,
      };
    case ClientActionTypes.CLIENT_UPDATE_CLIENT_SUCCESS:
      let updateClientResult: Result<Client> = action.payload;
      let clientsWithUpdatedClient = state.clients.map(client => {
        return client.id == updateClientResult.data?.id ? updateClientResult.data : client;
      });
      return {
        ...state,
        clientUpdateTaskStatus: TaskStatus.completed,
        clients: [...clientsWithUpdatedClient]
      };
    case ClientActionTypes.CLIENT_UPDATE_CLIENT_FAILURE:
      return {
        ...state,
        clientUpdateTaskStatus: TaskStatus.failed,
        // error: action.payload,
      };
    case ClientActionTypes.CLIENT_UPDATE_CLIENT_FINISH:
      return {
        ...state,
        clientUpdateTaskStatus: TaskStatus.idle,
        // error: action.payload,
      };
    case ClientActionTypes.CLIENT_CREATE_CLIENT_START:
      return {
        ...state,
        clientCreateTaskStatus: TaskStatus.running,
      };
    case ClientActionTypes.CLIENT_CREATE_CLIENT_SUCCESS:
      let createClientResult: Result<Client> = action.payload;
      let clientsWithCreatedClient = [createClientResult.data, ...state.clients]
      return {
        ...state,
        clientCreateTaskStatus: TaskStatus.completed,
        clients: [...clientsWithCreatedClient]
      };
    case ClientActionTypes.CLIENT_CREATE_CLIENT_FAILURE:
      return {
        ...state,
        clientCreateTaskStatus: TaskStatus.failed,
        // error: action.payload,
      };
    case ClientActionTypes.CLIENT_CREATE_CLIENT_FINISH:
      return {
        ...state,
        clientCreateTaskStatus: TaskStatus.idle,
        // error: action.payload,
      };
    default:
      return state;
  }
}

export default clientReducer;