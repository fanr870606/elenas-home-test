import { takeLatest, put, all, call } from 'redux-saga/effects';
import ClientActionTypes from './client.types';
import {
  searchClientSuccess, searchClientFailure, updateClientSuccess, updateClientFailure, createClientSuccess, createClientFailure, updateClientFinish, createClientFinish, searchClientFinish
} from './client.actions';
import Result from '../../../domain/result/result';
import Client from '../../../domain/models/client';
import { ResultStatus } from '../../../domain/result/result-status.enum';
import { ClientSearchUseCaseContract } from '../../../domain/usecase/client/client_search_use_case';
import { ClientUpdateUseCaseContract } from '../../../domain/usecase/client/client_update_use_case';
import { ClientCreateUseCaseContract } from '../../../domain/usecase/client/client_create_use_case';
import { displayMessage } from '../message/message.sagas';
import diContainer from '../../../dependency-injection/inversify.config';
import { TYPES } from '../../../dependency-injection/types';

var clientSearchUseCase: ClientSearchUseCaseContract = diContainer.get<ClientSearchUseCaseContract>(TYPES.ClientSearchUseCaseContract);
var clientUpdateUseCase: ClientUpdateUseCaseContract = diContainer.get<ClientUpdateUseCaseContract>(TYPES.ClientUpdateUseCaseContract);
var clientCreateUseCase: ClientCreateUseCaseContract = diContainer.get<ClientCreateUseCaseContract>(TYPES.ClientCreateUseCaseContract);

export function* searchClient() {
  try {
    let result: Result<Client[]> = yield clientSearchUseCase.invoke();
    if (result.status == ResultStatus.ok) {
      yield put(searchClientSuccess(result));
    } else {
      yield put(searchClientFailure(result));
      yield displayMessage({ payload: { text: result.error } });
    }
  } catch (error) {
    yield displayMessage({ payload: { text: error.message } });
  } finally {
    yield put(searchClientFinish());
  }
}

export function* updateClient(params: any) {
  try {
    const { payload }: { payload: Client } = params;
    let result: Result<Client> = yield clientUpdateUseCase.invoke(payload);
    if (result.status == ResultStatus.ok) {
      yield put(updateClientSuccess(result));
    } else {
      yield put(updateClientFailure(result));
      yield displayMessage({ payload: { text: result.error } });
    }
  } catch (error) {
    yield displayMessage({ payload: { text: error.message } });
  } finally {
    yield put(updateClientFinish());
  }
}

export function* createClient(params: any) {
  try {
    const { payload }: { payload: Client } = params;
    let result: Result<Client> = yield clientCreateUseCase.invoke(payload);
    if (result.status == ResultStatus.ok) {
      yield put(createClientSuccess(result));      
    } else {
      yield put(createClientFailure(result));
      yield displayMessage({ payload: { text: result.error } });
    }
  } catch (error) {
    yield displayMessage({ payload: { text: JSON.stringify(error) } });
  } finally {
    yield put(createClientFinish());
  }
}

export function* onSearchClientInStart() {
  yield takeLatest(ClientActionTypes.CLIENT_SEARCH_CLIENT_START, searchClient);
}

export function* onUpdateClientStart() {
  yield takeLatest(ClientActionTypes.CLIENT_UPDATE_CLIENT_START, updateClient);
}

export function* onCreateClientStart() {
  yield takeLatest(ClientActionTypes.CLIENT_CREATE_CLIENT_START, createClient);
}

export function* clientSagas() {
  yield all([
    call(onSearchClientInStart),
    call(onUpdateClientStart),
    call(onCreateClientStart),
  ]);
}
