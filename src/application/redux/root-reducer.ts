import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import { AsyncStorage } from 'react-native';

import authReducer from './auth/auth.reducer';
import clientReducer from './client/client.reducer';
import messageReducer from './message/message.reducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  auth: authReducer,
  client: clientReducer,
  message: messageReducer,
});

export default persistReducer(persistConfig, rootReducer);