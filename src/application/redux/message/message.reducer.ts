import MessageActionTypes from "./message.types";

const INITIAL_STATE =
{
  currentMessage: null,
  showMessage: false
}

const messageReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case MessageActionTypes.MESSAGE_SET_MESSAGE:
      return {
        ...state,
        currentMessage: action.payload,
        showMessage: true
      };
    case MessageActionTypes.MESSAGE_CLEAN_MESSAGE:
      return {
        ...state,
        currentMessage: INITIAL_STATE.currentMessage,
        showMessage: INITIAL_STATE.showMessage
      };
    default:
      return state;
  }
}

export default messageReducer;