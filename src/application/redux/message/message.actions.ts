import { IMessage } from "../../models/message"
import MessageActionTypes from "./message.types"

export const setMessage = (message: IMessage) => ({
  type: MessageActionTypes.MESSAGE_SET_MESSAGE,
  payload: message
})

export const cleanMessage = () => ({
  type: MessageActionTypes.MESSAGE_CLEAN_MESSAGE
})