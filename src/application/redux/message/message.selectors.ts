import { createSelector } from 'reselect';

const selectMessage = (state: any) => state.message;

export const selectCurrentMessage = createSelector(
  [selectMessage],
  message => message.currentMessage
);

export const selectShowMessage = createSelector(
  [selectMessage],
  message => message.showMessage
);