import { takeLatest, put, all, call, delay } from 'redux-saga/effects';
import { IMessage } from '../../models/message';
import { cleanMessage, setMessage } from './message.actions';
import MessageActionTypes from './message.types';

export function* displayMessage(params: any) {
  const { payload }: { payload: IMessage } = params;
  yield put(setMessage(payload))
  yield put(cleanMessage())
}

export function* onDisplayMessage() {
  yield takeLatest(MessageActionTypes.MESSAGE_DISPLAY_MESSAGE, displayMessage);
}

export function* userSagas() {
  yield all([
    call(onDisplayMessage),
  ]);
}