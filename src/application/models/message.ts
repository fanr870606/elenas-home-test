export interface IMessage {
  text: string,
  messageType: "danger" | "success" | "warning",
}