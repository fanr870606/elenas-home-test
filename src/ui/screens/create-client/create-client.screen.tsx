import { Body, Container, Content, Header, Title } from 'native-base'
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Client from '../../../domain/models/client'
import { createClientStart } from '../../../application/redux/client/client.actions';
import { selectClientCreateTaskStatus } from '../../../application/redux/client/client.selectors';
import { useNavigation } from '@react-navigation/native';
import Routes from '../../navigation/routes';
import { TaskStatus } from '../../../application/redux/task-status';
import ClientForm from '../../forms/client.form';

interface Props {
  client: Client,
  clientCreateTaskStatus: TaskStatus,
  createClientStart(client: Client): void
}

const CreateClientScreen = (props: Props) => {
  const { clientCreateTaskStatus, createClientStart } = props;
  const navigation = useNavigation();

  useEffect(() => {
    if (clientCreateTaskStatus == TaskStatus.completed) {
      if (navigation.canGoBack()) {
        navigation.goBack();
      } else {
        navigation.navigate(Routes.SearchClient);
      }
    }
  }, [clientCreateTaskStatus]);

  const onSubmitForm = (newClient: Client) => {
    createClientStart(newClient);
  }

  return (
    <Container>
      <Header>
        <Body>
          <Title>Create Client</Title>
        </Body>
      </Header>
      <Content>
        <ClientForm onSubmitForm={onSubmitForm} isExecuting={clientCreateTaskStatus == TaskStatus.running} />
      </Content>
    </Container>
  )
}

const mapStateToProps = createStructuredSelector({
  clientCreateTaskStatus: selectClientCreateTaskStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  createClientStart: (client: Client) =>
    dispatch(createClientStart(client))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateClientScreen);
