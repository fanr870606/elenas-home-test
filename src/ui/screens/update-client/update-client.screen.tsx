import { Body, Container, Content, Header, Title } from 'native-base'
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Client from '../../../domain/models/client'
import { updateClientStart } from '../../../application/redux/client/client.actions';
import { selectClientUpdateTaskStatus, selectSelectedClient } from '../../../application/redux/client/client.selectors';
import { useNavigation } from '@react-navigation/native';
import Routes from '../../navigation/routes';
import { TaskStatus } from '../../../application/redux/task-status';
import ClientForm from '../../forms/client.form';

interface Props {
  client: Client,
  clientUpdateTaskStatus: TaskStatus,
  updateClientStart(client: Client): void
}

const UpdateClientScreen = (props: Props) => {
  const { client, clientUpdateTaskStatus, updateClientStart } = props;
  const navigation = useNavigation();

  useEffect(() => {
    if (clientUpdateTaskStatus == TaskStatus.completed) {
      if (navigation.canGoBack()) {
        navigation.goBack();
      } else {
        navigation.navigate(Routes.SearchClient);
      }
    }
  }, [clientUpdateTaskStatus]);

  const onSubmitForm = (updatedClient: Client) => {
    updateClientStart({ ...updatedClient, id: client.id });
  }

  return (
    <Container>
      <Header>
        <Body>
          <Title>Update Client</Title>
        </Body>
      </Header>
      <Content>
        <ClientForm client={client} onSubmitForm={onSubmitForm} isExecuting={clientUpdateTaskStatus == TaskStatus.running} />
      </Content>
    </Container>
  )
}

const mapStateToProps = createStructuredSelector({
  clientUpdateTaskStatus: selectClientUpdateTaskStatus,
  client: selectSelectedClient,
});

const mapDispatchToProps = (dispatch: any) => ({
  updateClientStart: (client: Client) =>
    dispatch(updateClientStart(client))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateClientScreen);
