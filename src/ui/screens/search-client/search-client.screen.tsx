import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Body, Text, Right, Icon, Container, Header, Title, Button, Spinner } from 'native-base';
import React, { useEffect } from 'react'
import { FlatList } from 'react-native'
import Client from '../../../domain/models/client';
import { selectClients, selectClientSearchTaskStatus } from '../../../application/redux/client/client.selectors';
import { searchClientStart, selectClient } from '../../../application/redux/client/client.actions';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Routes from '../../navigation/routes';
import ClientCard from '../../components/client-card/client-card.component';
import { TaskStatus } from '../../../application/redux/task-status';

interface Props {
  clientSearchTaskStatus: TaskStatus,
  searchClientStart(): void,
  selectClient(client: Client): void,
  clients: Client[],
}

const SearchClientScreen = (props: Props) => {
  const navigation = useNavigation();
  const { clients, clientSearchTaskStatus } = props;

  useEffect(() => {
    props.searchClientStart();
  }, []);

  const handleRefresh = () => {
    props.searchClientStart();
  }

  const onNewClientCTAPress = () => {
    navigation.navigate(Routes.CreateClient);
  }

  const onClientSelected = (client: Client) => {
    props.selectClient(client);
    navigation.navigate(Routes.UpdateClient);
  }

  return (
    <Container>
      <Header>
        <Body>
          <Title>Clients</Title>
        </Body>
        <Right>
          <TouchableOpacity onPress={onNewClientCTAPress}>
            <Button transparent>
              <Text>Add Client</Text>
              <Icon name='add' />
            </Button>
          </TouchableOpacity>
        </Right>
      </Header>
      {Array.isArray(clients) && <FlatList
        refreshing={clientSearchTaskStatus == TaskStatus.running}
        onRefresh={handleRefresh}
        data={clients}
        renderItem={({ item }) => <ClientCard client={item} selectClient={onClientSelected} />}
        keyExtractor={(item) => item.id?.toString()}
      />}
    </Container>
  )
}

const mapStateToProps = createStructuredSelector({
  clientSearchTaskStatus: selectClientSearchTaskStatus,
  clients: selectClients,
});

const mapDispatchToProps = (dispatch: any) => ({
  searchClientStart: () =>
    dispatch(searchClientStart()),
  selectClient: (client: Client) =>
    dispatch(selectClient(client)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchClientScreen);
