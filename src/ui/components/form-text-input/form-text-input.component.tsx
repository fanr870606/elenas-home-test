import { Icon, Input, Item, Label } from 'native-base'
import React from 'react'
import { View, Text } from 'react-native'

interface Props {
  label?: string,
  value?: string,
  onBlur?: any,
  onChangeText?: any,
  error?: boolean,
  errorMessage?: string,
  maxLength?: number
}

const FormTextInput = (props: Props) => {
  const { onBlur, onChangeText, value, maxLength, label, error } = props;
  return (
    <Item stackedLabel error={error}>
      {label && <Label>{label}</Label>}
      <Input
        onBlur={onBlur}
        onChangeText={value => onChangeText(value)}
        value={value}
        maxLength={maxLength ?? 50}
      />
      {error && <Icon light name='close-circle' style={{ color: "red" }} />}
    </Item>
  )
}

export default FormTextInput
