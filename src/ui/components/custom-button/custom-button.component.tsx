import React from 'react'
import { Button, Text, Spinner } from 'native-base'

interface Props {
  isExecuting?: boolean,
  onPress: any,
  children?: any,
  style?: any
}


const CustomButton = (props: Props) => {
  const { isExecuting, ...rest } = props;

  if (isExecuting) {
    return (
      <Button rounded full success {...rest}>
        <Spinner color="white" />
      </Button>
    )
  }

  return (
    <Button rounded full success {...rest}>
      {props.children}
    </Button>
  )
}

export default CustomButton;
