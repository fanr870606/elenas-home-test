import { Body, Icon, Left, ListItem, Right, Text } from "native-base";
import React from "react";
import { TouchableOpacity } from "react-native";
import Client from "../../../domain/models/client";

interface ClientCardProps {
  client: Client,
  selectClient(client: Client): void
}

const ClientCard = (props: ClientCardProps) => {
  const { client, selectClient } = props;

  return (
    <ListItem avatar>
      <Left>
        <Icon name='person-circle' style={{ color: 'green', fontSize: 40, width: 40 }} />
      </Left>
      <Body>
        <Text>{client.firstName} {client.lastName}</Text>
        {client.city && <Text note>City: {client.city}</Text>}
        {client.address && <Text note>Address: {client.address}</Text>}
        {client.cellphone && <Text note>Cellphone: {client.cellphone}</Text>}
        {client.cedula && <Text note>CC: {client.cedula}</Text>}
      </Body>
      <Right>
        <TouchableOpacity onPress={() => selectClient(client)} style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
          <Icon name='create-outline' style={{ color: 'blue', fontSize: 40, width: 40 }} />
          <Text>Edit</Text>
        </TouchableOpacity>
      </Right>
    </ListItem>
  )
}

export default ClientCard
