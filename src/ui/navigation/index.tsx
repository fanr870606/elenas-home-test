import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useEffect } from "react";
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { StatusBar } from "react-native";
import CreateClientScreen from "../screens/create-client/create-client.screen";
import SearchClientScreen from "../screens/search-client/search-client.screen";
import UpdateClientScreen from "../screens/update-client/update-client.screen";
import Routes from "./routes";
import { selectCurrentMessage, selectShowMessage } from "../../application/redux/message/message.selectors";
import { IMessage } from "../../application/models/message";
import { Toast } from "native-base";

interface Props {
  showMessage?: boolean,
  message?: IMessage
}

const Stack = createStackNavigator();

function Navigation(props: Props) {
  const { showMessage, message } = props;

  useEffect(() => {
    if (showMessage) {
      Toast.show({ text: message?.text ?? '', duration: 5000, type: message?.messageType ?? 'danger', position: 'bottom' });
    }
  }, [showMessage])

  return (
    <NavigationContainer>
      <AuthenticatedNavigator />
      <StatusBar />
    </NavigationContainer>
  );
}

function AuthenticatedNavigator() {
  return (
    <Stack.Navigator initialRouteName={Routes.SearchClient} headerMode="none">
      <Stack.Screen name={Routes.SearchClient} component={SearchClientScreen} />
      <Stack.Screen name={Routes.CreateClient} component={CreateClientScreen} />
      <Stack.Screen name={Routes.UpdateClient} component={UpdateClientScreen} />
    </Stack.Navigator>
  );
}

const mapStateToProps = createStructuredSelector({
  showMessage: selectShowMessage,
  message: selectCurrentMessage
});

export default connect(
  mapStateToProps,
  null
)(Navigation);