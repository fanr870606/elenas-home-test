interface RoutesProps {
  SignIn: string,
  SearchClient: string,
  CreateClient: string,
  UpdateClient: string,
}

const Routes: RoutesProps = {
  SignIn: 'SignIn',
  SearchClient: 'ClientSearch',
  CreateClient: 'CreateClient',
  UpdateClient: 'UpdateClient',
}

export default Routes