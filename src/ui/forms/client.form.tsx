import { Button, Form, Icon, Text } from 'native-base';
import React from 'react'
import { useForm, Controller } from "react-hook-form";
import Client from '../../domain/models/client'
import CustomButton from '../components/custom-button/custom-button.component';
import FormTextInput from '../components/form-text-input/form-text-input.component';

interface Props {
  client?: Client,
  onSubmitForm(client: Client): void,
  isExecuting?: boolean
}

const ClientForm = (props: Props) => {
  const { client, onSubmitForm, isExecuting } = props;
  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = (data: any) => {
    onSubmitForm(data);
  };

  return (
    <Form>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <FormTextInput
            label={"First Name"}
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            error={!!errors.firstName}
          />
        )}
        name="firstName"
        rules={{ required: true }}
        defaultValue={client?.firstName}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <FormTextInput
            label={"Last Name"}
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            error={!!errors.lastName} />
        )}
        name="lastName"
        rules={{ required: true }}
        defaultValue={client?.lastName}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <FormTextInput
            label={"Document"}
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            error={!!errors.cedula} />
        )}
        name="cedula"
        rules={{ required: true }}
        defaultValue={client?.cedula}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <FormTextInput
            label={"Cellphone"}
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            error={!!errors.cellphone} />
        )}
        name="cellphone"
        rules={{ required: true }}
        defaultValue={client?.cellphone}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <FormTextInput
            label={"Address"}
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            error={!!errors.address} />
        )}
        name="address"
        rules={{ required: true }}
        defaultValue={client?.address}
      />
      <CustomButton isExecuting={isExecuting} style={{ alignSelf: "center", marginTop: 20, marginHorizontal: 20 }}
        onPress={handleSubmit(onSubmit)}>
        <Icon name='save-outline' />
        <Text>{!!client ? "Update Client" : "Create Client"}</Text>
      </CustomButton>
    </Form>
  );
}

export default ClientForm
