import "reflect-metadata";
import AppLoading from 'expo-app-loading';
import { Root } from 'native-base';
import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context';
import PersistProvider from './application/providers/persist-provider';
import useCachedResources from './ui/hooks/useCachedResources';
import Navigation from './ui/navigation'

interface Props {

}

const App = (props: Props) => {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return <AppLoading />;
  }

  return (
    <PersistProvider>
      <SafeAreaProvider>
        <Root>
          <Navigation />
        </Root>
      </SafeAreaProvider >
    </PersistProvider>
  );
}

export default App
