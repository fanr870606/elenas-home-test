# Prueba Técnica CRUD Clientes

## Ejecutar el proyecto

Para desplegar el proyecto se debe ejecutar el siguiente comando desde la raíz del proyecto

```bash
expo start
```

## Ejecutar grupo de pruebas

Para correr el conjunto de pruebas se debe ejecutar el siguiente comando desde la raíz del proyecto

```bash
yarn jest
```

## Arquitectura del proyecto

El proyecto fue construido bajo el paradigma Clean Arquitecture con el objetivo de aislar el dominio de los detalles tecnológicos 

## Estructura de carpetas

El proyecto esta creado siguiendo la siguiente estructura de carpetas

### src/ui

Carpeta donde se encuentra todo lo referente a la interfaz gráfica de la aplicación

### src/domain

Carpeta donde se encuentran las entidades, casos de uso y contratos de repositorio

### src/dependency-injection

Carpeta donde se encuentran la configuración del contenedor de inyección de dependencias

### src/data

Carpeta donde se encuentran la logica de acceso a datos

### src/application

Carpeta donde se encuentra la configuración de proovedores de estado y de persistencia.

### __test__

Carpeta donde se encuentran las pruebas unitarias y pruebas de componentes ui

## Pantallas

### Search Client

Muestra un listado de los clientes devueltos por el servicio

### Create Client

Permite la creación de un cliente mediante un formulario

### Update Client

Permite la actualización de un cliente seleccionado desde la pantalla de listado de clientes mediante un formulario

## Dependencias Usadas

- native-base: Componentes gráficos
- inversify: Inyección de dependencias
- axios: Consumo de servicios
- redux - redux-saga: Gestión de estado y manejo de side effects
- jest - enzyme: Construcción de pruebas unitarias y de ui